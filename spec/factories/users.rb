# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name "MyText"

    factory :user_ichiro do
      name "Ichiro"
    end

    factory :user_jiro do
      name "Jiro"
    end

    factory :user_saburou do
      name "Saburou"
    end
  end
end
