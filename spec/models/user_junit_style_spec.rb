# -*- encoding: utf-8 -*-
require 'spec_helper'

describe User do

  context "に2件のレコードがある場合" do
    before do
      FactoryGirl.create(:user_ichiro)
      FactoryGirl.create(:user_jiro)
    end

    it "2件取得できる事" do
      # Exercise
      actual = User.all

      # Verify
      actual.count.should == 2
      actual[0].name == "Ichiro"
      actual[1].name == "Jiro"
    end

    it "1件追加できる" do
      # Exercise
      User.create(:name => "Saburou")

      # Verify
      User.count.should == 3
      User.exists?(:name => "Saburou").should be_true
    end
  end

  context "に0件のレコードがある場合" do
    it "0件取得できる事" do
      # Exercise
      actual = User.all

      # Verify
      actual.count.should == 0
    end

    it "1件追加できる" do
      # Exercise
      User.create(:name => "Sirou")

      # Verify
      User.count.should == 1
      User.exists?(:name => "Sirou").should be_true
    end
  end
end
