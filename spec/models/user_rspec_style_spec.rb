# -*- encoding: utf-8 -*-
require 'spec_helper'

describe User do

  subject{ User.all }

  context "に2件のレコードがある場合" do
    before do
      @ichiro = FactoryGirl.create(:user_ichiro)
      @jiro = FactoryGirl.create(:user_jiro)
    end

    describe "2件取得できる事" do
      # この2つは同じテストケース
      it{ should have_exactly(2).users }
      its(:count){ should == 2 }

      it{ should include @ichiro }
      it{ should include @jiro }
    end

    describe "1件追加できる" do
      before do
        # Exercise
        @saburou = User.create(:name => "Saburou")
      end

      it{ should have_exactly(3).users }
      it{ should include @saburou  }
    end
  end

  context "に0件のレコードがある場合" do
    describe "0件取得できる事" do
      it{ should have_exactly(0).users }
    end

    describe "1件追加できる" do
      before do
        # Exercise
        @sirou = User.create(:name => "Sirou")
      end

      it{ should have_exactly(1).users }
      it{ should include @sirou  }
    end
  end
end
